#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
# Ryuunosuke Ayanokouzi <i38w7i3@yahoo.co.jp>, 2015-2019.
# Kenshi Muto <kmuto@kmuto.jp>, 2015.
# Yoichi Chonan <cyoichi@maple.ocn.ne.jp>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2019-02-03 09:00+0900\n"
"PO-Revision-Date: 2019-02-03 09:00+0900\n"
"Last-Translator: AYANOKOUZI, Ryuunosuke <i38w7i3@yahoo.co.jp>\n"
"Language-Team: Japanese <https://github.com/l/debian-handbook/tree/master/translation/ja_JP/push>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Live CD"
msgstr "ライブ CD"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Specificities"
msgstr "特徴"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Particular Choices"
msgstr "好み"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Derivative Distributions"
msgstr "派生ディストリビューション"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<indexterm><primary>derivative distribution</primary></indexterm> <indexterm><primary>distribution, derivative</primary></indexterm> Many Linux distributions are derivatives of Debian and reuse Debian's package management tools. They all have their own interesting properties, and it is possible one of them will fulfill your needs better than Debian itself."
msgstr "<indexterm><primary sortas=\"ハセイディストリビューション\">派生ディストリビューション</primary></indexterm><indexterm><primary>ディストリビューション</primary><secondary sortas=\"ハセイ\">派生</secondary></indexterm>多くの Linux ディストリビューションは Debian の派生物で、Debian のパッケージ管理ツールを再利用しています。派生ディストリビューションは興味深い特徴を備えており、Debian そのものよりもあなたの要求を満足させる場合があります。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Census and Cooperation"
msgstr "派生物調査と協力体制"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "The Debian project fully acknowledges the importance of derivative distributions and actively supports collaboration between all involved parties. This usually involves merging back the improvements initially developed by derivative distributions so that everyone can benefit and long-term maintenance work is reduced."
msgstr "Debian プロジェクトは派生ディストリビューションの重要性を完全に認めており、関係者間の協力を活発に支援しています。通常これは、当初派生ディストリビューションで開発されていた改善を Debian が取り込むことを意味します。こうすることで、誰もが恩恵を受けることができ、長期におよぶメンテナンス作業を減らすことになります。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "This explains why derivative distributions are invited to become involved in discussions on the <literal>debian-derivatives@lists.debian.org</literal> mailing-list, and to participate in the derivative census. This census aims at collecting information on work happening in a derivative so that official Debian maintainers can better track the state of their package in Debian variants. <ulink type=\"block\" url=\"https://wiki.debian.org/DerivativesFrontDesk\" /><ulink type=\"block\" url=\"https://wiki.debian.org/Derivatives/Census\" />"
msgstr "このため、派生ディストリビューションは <literal>debian-derivatives@lists.debian.org</literal> メーリングリストの議論に参加し、派生物調査に参加するよう勧められます。派生物調査は派生ディストリビューションの中でなされた作業に関する情報を集めることを目標にしています。こうすることで、公式の Debian 開発者が Debian 派生物内の自分のパッケージの状況をより良く追跡することが可能になります。<ulink type=\"block\" url=\"https://wiki.debian.org/DerivativesFrontDesk\" /><ulink type=\"block\" url=\"https://wiki.debian.org/Derivatives/Census\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Let us now briefly describe the most interesting and popular derivative distributions."
msgstr "それでは、最も興味深く人気のある派生ディストリビューションを簡単に説明していきましょう。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Ubuntu"
msgstr "Ubuntu"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Ubuntu</primary>"
msgstr "<primary>Ubuntu</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
# Tag: PTAL;
msgid "Ubuntu made quite a splash when it came on the Free Software scene, and for good reason: Canonical Ltd., the company that created this distribution, started by hiring thirty-odd Debian developers and publicly stating the far-reaching objective of providing a distribution for the general public with a new release twice a year. They also committed to maintaining each version for a year and a half."
msgstr "Ubuntu がフリーソフトウェア世界に登場した時、Ubuntu は一大旋風を巻き起こしました。これは次の正当な理由があります。なぜなら、Ubuntu ディストリビューションを作った企業である Canonical Ltd. は 30 人程度の Debian 開発者を雇用して創設されており、そして 1 年に 2 回新リリースを行う一般人向けのディストリビューションを提供するという大規模な目標を公式に宣言したからです。さらに Canonical Ltd. はそれぞれのバージョンを 18 カ月間メンテナンスすることも表明しました。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "These objectives necessarily involve a reduction in scope; Ubuntu focuses on a smaller number of packages than Debian, and relies primarily on the GNOME desktop (although an official Ubuntu derivative, called “Kubuntu”, relies on KDE Plasma). Everything is internationalized and made available in a great many languages."
msgstr "これらの目標を達成するために対象範囲を狭める必要がありました。このため Ubuntu は Debian よりも数少ないパッケージに重点的に取り組んでおり、主として GNOME デスクトップに依存しています (しかしながら、公式の Ubuntu 派生物である「Kubuntu」は KDE Plasma に依存しています)。すべては国際化されており、数多くの言語で利用できるようにされています。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Kubuntu</primary>"
msgstr "<primary>Kubuntu</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "So far, Ubuntu has managed to keep this release rhythm. They also publish <emphasis>Long Term Support</emphasis> (LTS) releases, with a 5-year maintenance promise. As of April 2015, the current LTS version is version 14.04, nicknamed Utopic Unicorn. The last non-LTS version is version 15.04, nicknamed Vivid Vervet. Version numbers describe the release date: 15.04, for example, was released in April 2015."
msgstr "今のところ、Ubuntu はリリース周期を保つように管理されています。Ubuntu は<emphasis>長期サポート</emphasis> (LTS) リリースを公開しており、これは 5 年間のメンテナンス期間を保証しています。2015 年 4 月の時点で、現在の LTS 版は Utopic Unicorn という愛称のバージョン 14.04、最新の通常版は Vivid Vervet という愛称の 15.04 です。バージョン番号はリリース日を表しています。たとえば、15.04 は 2015 年 4 月にリリースされたことを意味しています。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<emphasis>IN PRACTICE</emphasis> Ubuntu's support and maintenance promise"
msgstr "<emphasis>IN PRACTICE</emphasis> Ubuntu のサポートとメンテナンス保証"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary><literal>main</literal></primary>"
msgstr "<primary><literal>main</literal></primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary><literal>restricted</literal></primary>"
msgstr "<primary><literal>restricted</literal></primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary><literal>universe</literal></primary>"
msgstr "<primary><literal>universe</literal></primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary><literal>multiverse</literal></primary>"
msgstr "<primary><literal>multiverse</literal></primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Canonical has adjusted multiple times the rules governing the length of the period during which a given release is maintained. Canonical, as a company, promises to provide security updates to all the software available in the <literal>main</literal> and <literal>restricted</literal> sections of the Ubuntu archive, for 5 years for LTS releases and for 9 months for non-LTS releases. Everything else (available in the <literal>universe</literal> and <literal>multiverse</literal>) is maintained on a best-effort basis by volunteers of the MOTU team (<emphasis>Masters Of The Universe</emphasis>). Be prepared to handle security support yourself if you rely on packages of the latter sections."
msgstr "Canonical は対象のリリースのメンテナンス期間の長さを決定する規則を何回も調整しました。Canonical は企業として Ubuntu アーカイブの <literal>main</literal> と <literal>restricted</literal> セクションに含まれるすべてのソフトウェアに対するセキュリティ更新を提供することを保証しています。セキュリティ更新が提供される期間は LTS リリースの場合 5 年間、非 LTS リリースの場合 9 カ月間です。それ以外 (<literal>universe</literal> と <literal>multiverse</literal> セクションに含まれるソフトウェア) は MOTU チーム (<emphasis>Masters Of The Universe</emphasis>) のボランティアが最善の努力を行うという条件の下でメンテナンスされます。<literal>universe</literal> と <literal>multiverse</literal> セクションに含まれるパッケージを使っている場合、自分の手でセキュリティサポートを取り扱う覚悟が必要です。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Ubuntu has reached a wide audience in the general public. Millions of users were impressed by its ease of installation, and the work that went into making the desktop simpler to use."
msgstr "Ubuntu は一般人に広く支持されるようになりました。数百万のユーザが Ubuntu のインストールの簡単さとデスクトップを簡単に使えるようにするという Ubuntu の業績に感銘を受けました。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Ubuntu and Debian used to have a tense relationship; Debian developers who had placed great hopes in Ubuntu contributing directly to Debian were disappointed by the difference between the Canonical marketing, which implied Ubuntu were good citizens in the Free Software world, and the actual practice where they simply made public the changes they applied to Debian packages. Things have been getting better over the years, and Ubuntu has now made it general practice to forward patches to the most appropriate place (although this only applies to external software they package and not to the Ubuntu-specific software such as Mir or Unity). <ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"
msgstr "過去、Ubuntu と Debian には緊張関係がありました。そして Ubuntu が Debian に対して直接的に貢献することに大きな希望を託した Debian 開発者は Canonical のマーケティング (これは Ubuntu はフリーソフトウェア世界の善良な市民だったことを暗黙のうちに示していました) と実践方法 (Ubuntu は Debian パッケージに対して適用した変更を公開するに過ぎませんでした) の違いに落胆しました。年月を重ねるに連れて状況は改善しつつあり、現在の Ubuntu はパッチを最も適切な場所に送付するという慣行をよく守っています (しかしながら、この慣行は Ubuntu がパッケージングを行った外部ソフトウェアだけに適用され、Mir や Unity などの Ubuntu 特有のソフトウェアには適用されていません)。<ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Linux Mint"
msgstr "Linux Mint"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Linux Mint</primary>"
msgstr "<primary>Linux Mint</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Linux Mint is a (partly) community-maintained distribution, supported by donations and advertisements. Their flagship product is based on Ubuntu, but they also provide a “Linux Mint Debian Edition” variant that evolves continuously (as it is based on Debian Testing). In both cases, the initial installation involves booting a LiveDVD."
msgstr "Linux Mint は (部分的に) コミュニティによってメンテナンスされているディストリビューションで、寄付と広告によって支えられています。Linux Mint の主力成果物は Ubuntu を基にしていますが、Linux Mint は連続的に進化し続ける「Linux Mint Debian Edition」という亜種も提供しています (これは Debian テスト版を基にしています)。どちらの場合も、最初のインストールはライブ DVD を起動することから始まります。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "The distribution aims at simplifying access to advanced technologies, and provides specific graphical user interfaces on top of the usual software. For instance, Linux Mint relies on Cinnamon instead of GNOME by default (but it also includes MATE as well as Plasma and Xfce); similarly, the package management interface, although based on APT, provides a specific interface with an evaluation of the risk from each package update."
msgstr "Linux Mint ディストリビューションは先進的な技術を簡単に使えるようにすることを目標としており、一般的なソフトウェアに対する特別なグラフィカルユーザインターフェースを提供しています。たとえば、Linux Mint はデフォルトで GNOME ではなく Cinnamon を採用しています (ただし MATE および KDE Plasma と Xfce も提供しています)。同様に、Linux Mint のパッケージ管理インターフェースは APT に基づいていますが、各パッケージを更新する場合の危険性を評価する特別なインターフェースを提供しています。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Linux Mint includes a large amount of proprietary software to improve the experience of users who might need those. For example: Adobe Flash and multimedia codecs. <ulink type=\"block\" url=\"http://www.linuxmint.com/\" />"
msgstr "Linux Mint には数多くのプロプライエタリソフトウェアが含まれており、このことによりプロプライエタリソフトウェアを必要とするユーザのユーザ体験が向上します。Linux Mint に含まれるプロプライエタリソフトウェアの一例として、Adobe Flash やマルチメディアコーデックなどがあります。<ulink type=\"block\" url=\"http://www.linuxmint.com/\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Knoppix"
msgstr "Knoppix"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary><foreignphrase>LiveCD</foreignphrase></primary>"
msgstr "<primary><foreignphrase>ライブ CD</foreignphrase></primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Knoppix</primary>"
msgstr "<primary>Knoppix</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>bootable CD-ROM</primary>"
msgstr "<primary sortas=\"キドウカノウナ CD-ROM\">起動可能な CD-ROM</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>CD-ROM</primary><secondary>bootable</secondary>"
msgstr "<primary>CD-ROM</primary><secondary sortas=\"キドウカノウナ CD-ROM\">起動可能な CD-ROM</secondary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "The Knoppix distribution barely needs an introduction. It was the first popular distribution to provide a <emphasis>LiveCD</emphasis>; in other words, a bootable CD-ROM that runs a turn-key Linux system with no requirement for a hard-disk — any system already installed on the machine will be left untouched. Automatic detection of available devices allows this distribution to work in most hardware configurations. The CD-ROM includes almost 2 GB of (compressed) software, and the DVD-ROM version has even more."
msgstr "Knoppix ディストリビューションに関してほとんど紹介する必要はないでしょう。Knoppix は<emphasis>ライブ CD</emphasis> を提供する最初の人気ディストリビューションでした。言い換えれば、<emphasis>ライブ CD</emphasis> とはハードディスクの状態に依存せずにすぐに使える Linux システムを実行する起動可能な CD-ROM です。すなわち、マシンにインストール済みのいかなるシステムにも影響をおよぼしません。Knoppix ディストリビューションは、利用できるデバイスの自動検出機能を使うことで、多くのハードウェア構成で動きます。CD-ROM にはおよそ 2 GB の (圧縮された) ソフトウェアが含まれ、DVD-ROM 版の場合収録されているソフトウェアのサイズはより大きくなります。"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Combining this CD-ROM to a USB stick allows carrying your files with you, and to work on any computer without leaving a trace — remember that the distribution doesn't use the hard-disk at all. Knoppix uses LXDE (a lightweight graphical desktop) by default, but the DVD version also includes GNOME and Plasma. Many other distributions provide other combinations of desktops and software. This is, in part, made possible thanks to the <emphasis role=\"pkg\">live-build</emphasis> Debian package that makes it relatively easy to create a LiveCD. <ulink type=\"block\" url=\"http://live.debian.net/\" />"
msgstr "Knoppix の CD-ROM を USB メモリに組み込むことで、ファイルと一緒にシステムを持ち歩いて、コンピュータに痕跡を残さずに作業することが可能になります。つまり、Knoppix ディストリビューションはハードディスクを全く使いません。Knoppix はデフォルトで LXDE (軽量のグラフィカルデスクトップ) を採用していますが、DVD 版では GNOME と Plasma も収録しています。さらに Knoppix の他の多くのディストリビューションでは、デスクトップとソフトウェアの他の組み合わせも提供されています。これの実現には、ライブ CD の作成を比較的簡単に行うことを可能にする <emphasis role=\"pkg\">live-build</emphasis> Debian パッケージが寄与しています。<ulink type=\"block\" url=\"http://live.debian.net/\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary><emphasis role=\"pkg\">live-build</emphasis></primary>"
msgstr "<primary><emphasis role=\"pkg\">live-build</emphasis></primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Note that Knoppix also provides an installer: you can first try the distribution as a LiveCD, then install it on a hard-disk to get better performance. <ulink type=\"block\" url=\"http://www.knopper.net/knoppix/index-en.html\" />"
msgstr "Knoppix はインストーラを備えている点に注意してください。すなわち Knoppix ディストリビューションをライブ CD で試した後に、性能を向上させる目的で Knoppix をハードディスクにインストールすることも可能です。<ulink type=\"block\" url=\"http://www.knopper.net/knoppix/index-en.html\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Aptosid and Siduction"
msgstr "Aptosid と Siduction"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Sidux</primary>"
msgstr "<primary>Sidux</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Aptosid</primary>"
msgstr "<primary>Aptosid</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Siduction</primary>"
msgstr "<primary>Siduction</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "These community-based distributions track the changes in Debian <emphasis role=\"distribution\">Sid</emphasis> (<emphasis role=\"distribution\">Unstable</emphasis>) — hence their name. The modifications are limited in scope: the goal is to provide the most recent software and to update drivers for the most recent hardware, while still allowing users to switch back to the official Debian distribution at any time. Aptosid was previously known as Sidux, and Siduction is a more recent fork of Aptosid. <ulink type=\"block\" url=\"http://aptosid.com\" /> <ulink type=\"block\" url=\"http://siduction.org\" />"
msgstr "Aptosid と Siduction はコミュニティベースのディストリビューションであり、Debian <emphasis role=\"distribution\">Sid</emphasis> (<emphasis role=\"distribution\">不安定版</emphasis>) の変更に追従しています。Aptosid や Siduction という名前はこの特徴から採られています。修正は狭い範囲に限定されています。つまり、Aptosid と Siduction の目標は最新のソフトウェアを提供し、最新のハードウェア用のドライバを更新することに限定されています。一方でユーザは何時でも公式の Debian ディストリビューションに切り替えることが可能です。以前 Aptosid は Sidux として知られていました。Siduction は Aptosid に対するより最近のフォークです。<ulink type=\"block\" url=\"http://aptosid.com\" /><ulink type=\"block\" url=\"http://siduction.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Grml"
msgstr "Grml"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Grml</primary>"
msgstr "<primary>Grml</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Grml is a LiveCD with many tools for system administrators, dealing with installation, deployment, and system rescue. The LiveCD is provided in two flavors, <literal>full</literal> and <literal>small</literal>, both available for 32-bit and 64-bit PCs. Obviously, the two flavors differ by the amount of software included and by the resulting size. <ulink type=\"block\" url=\"https://grml.org\" />"
msgstr "Grml はシステムのインストール、配備、レスキューを担当しているシステム管理者用の多くのツールを備えるライブ CD です。Grml のライブ CD は <literal>full</literal> と <literal>small</literal> の 2 種類あり、さらに 32 ビットと 64 ビット PC 用の CD が用意されています。言うまでもなく、<literal>full</literal> と <literal>small</literal> の違いは収録されているソフトウェアの数で、この違いがサイズに反映されています。<ulink type=\"block\" url=\"https://grml.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Tails"
msgstr "Tails"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Tails</primary>"
msgstr "<primary>Tails</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Tails (The Amnesic Incognito Live System) aims at providing a live system that preserves anonymity and privacy. It takes great care in not leaving any trace on the computer it runs on, and uses the Tor network to connect to the Internet in the most anonymous way possible. <ulink type=\"block\" url=\"https://tails.boum.org\" />"
msgstr "Tails (The Amnesic Incognito Live System) の目標は匿名性と秘匿性を守るライブシステムを提供することです。Tails は自分が実行されたコンピュータにいかなる痕跡も残さないように細心の注意を払い、最も匿名性の高い方法で Internet に接続するために Tor ネットワークを使います。<ulink type=\"block\" url=\"https://tails.boum.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Kali Linux"
msgstr "Kali Linux"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Kali</primary>"
msgstr "<primary>Kali</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>penetration testing</primary>"
msgstr "<primary>ペネトレーションテスト</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>forensics</primary>"
msgstr "<primary>フォレンジック</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Kali Linux is a Debian-based distribution specialising in penetration testing (“pentesting” for short). It provides software that helps auditing the security of an existing network or computer while it is live, and analyze it after an attack (which is known as “computer forensics”). <ulink type=\"block\" url=\"https://kali.org\" />"
msgstr "Kali Linux はペネトレーションテスト (略して「pentesting」) に特化した Debian ベースのディストリビューションです。Kali Linux は既存のネットワークや稼働中のコンピュータのセキュリティを監査するソフトウェアを提供し、攻撃を受けたコンピュータを解析します (「コンピュータフォレンジック」としても知られています)。<ulink type=\"block\" url=\"https://kali.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Devuan"
msgstr "Devuan"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Devuan</primary>"
msgstr "<primary>Devuan</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Devuan is a relatively new fork of Debian: it started in 2014 as a reaction to the decision made by Debian to switch to <command>systemd</command> as the default init system. A group of users attached to <command>sysv</command> and opposing (real or perceived) drawbacks to <command>systemd</command> started Devuan with the objective of maintaining a <command>systemd</command>-less system. As of March 2015, they haven't published any real release: it remains to be seen if the project will succeed and find its niche, or if the systemd opponents will learn to accept it. <ulink type=\"block\" url=\"https://devuan.org\" />"
msgstr "Devuan は Debian の比較的新しいフォークです。Devuan は Debian がデフォルトの init システムを <command>systemd</command> に切り替えるという決定に対する反発行動として 2014 年に開始されました。<command>sysv</command> に魅力を感じて <command>systemd</command> の (現実的および感覚的な) 欠点に反対するユーザのグループが <command>systemd</command> のないシステムをメンテナンスする目的で Devuan を開始しました。2015 年 3 月の時点で、Devuan グループはまだ実際のリリースを公開していません。つまり、Devuan プロジェクトが成功して活躍の場を見いだすのか、それとも systemd の対抗勢力が systemd を受け入れるのか、今のところまだわからないということです。<ulink type=\"block\" url=\"https://devuan.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Tanglu"
msgstr "Tanglu"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Tanglu</primary>"
msgstr "<primary>Tanglu</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Tanglu is another Debian derivative; this one is based on a mix of Debian <emphasis role=\"distribution\">Testing</emphasis> and <emphasis role=\"distribution\">Unstable</emphasis>, with patches to some packages. Its goal is to provide a modern desktop-friendly distribution based on fresh software, without the release constraints of Debian. <ulink type=\"block\" url=\"http://tanglu.org\" />"
msgstr "Tanglu はもう 1 つの Debian 派生物です。Tanglu は Debian <emphasis role=\"distribution\">テスト版</emphasis>と<emphasis role=\"distribution\">不安定版</emphasis>の混合物に基づいており、一部のパッケージにパッチを当てています。Tanglu の目標は、Debian のリリース条件に制限されることなく、新しいソフトウェアに基づく現代的なデスクトップに適したディストリビューションを提供することです。<ulink type=\"block\" url=\"http://tanglu.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "DoudouLinux"
msgstr "DoudouLinux"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>DoudouLinux</primary>"
msgstr "<primary>DoudouLinux</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "DoudouLinux targets young children (starting from 2 years old). To achieve this goal, it provides an heavily customized graphical interface (based on LXDE) and comes with many games and educative applications. Internet access is filtered to prevent children from visiting problematic websites. Advertisements are blocked. The goal is that parents should be free to let their children use their computer once booted into DoudouLinux. And children should love using DoudouLinux, just like they enjoy their gaming console. <ulink type=\"block\" url=\"http://www.doudoulinux.org\" />"
msgstr "DoudouLinux は幼児 (2 歳以上) を対象にしています。DoudouLinux は幼児を対象にしているため、大きくカスタマイズされたグラフィカルインターフェース (LXDE に基づきます) を提供し、多くのゲームと教育的アプリケーションを付属させています。インターネットアクセスは、子供に問題のあるウェブサイトを見せないように、フィルタされます。広告はブロックされます。DoudouLinux の目標は、コンピュータで DoudouLinux が起動したら、親は自分の子供に自由にそれを使わせることができることです。そして、子供はゲーム機のように DoudouLinux を使うのが大好きになることです。<ulink type=\"block\" url=\"http://www.doudoulinux.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Raspbian"
msgstr "Raspbian"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Raspbian</primary>"
msgstr "<primary>Raspbian</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Raspberry Pi</primary>"
msgstr "<primary>Raspberry Pi</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "Raspbian is a rebuild of Debian optimised for the popular (and inexpensive) Raspberry Pi family of single-board computers. The hardware for that platform is more powerful than what the Debian <emphasis role=\"architecture\">armel</emphasis> architecture can take advantage of, but lacks some features that would be required for <emphasis role=\"architecture\">armhf</emphasis>; so Raspbian is a kind of intermediary, rebuilt specifically for that hardware and including patches targeting this computer only. <ulink type=\"block\" url=\"https://raspbian.org\" />"
msgstr "Raspbian は Debian を人気の (安価な) シングルボードコンピュータである Raspberry Pi ファミリーに最適化して再ビルドしたディストリビューションです。Raspberry Pi 用のハードウェアは Debian の <emphasis role=\"architecture\">armel</emphasis> アーキテクチャで得られる利点を使うには強力ですが、<emphasis role=\"architecture\">armhf</emphasis> で要求されるであろういくつかの機能を欠いています。そんなわけで、Raspbian は両者の仲介者であり、Raspberry Pi のハードウェアに特化した再ビルドであり、Raspberry Pi コンピュータだけを対象にしたパッチを含んでいます。<ulink type=\"block\" url=\"https://raspbian.org\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "And Many More"
msgstr "その他"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "<primary>Distrowatch</primary>"
msgstr "<primary>Distrowatch</primary>"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "The Distrowatch website references a huge number of Linux distributions, many of which are based on Debian. Browsing this site is a great way to get a sense of the diversity in the Free Software world. <ulink type=\"block\" url=\"http://distrowatch.com\" />"
msgstr "Distrowatch ウェブサイトには数多くの Linux ディストリビューションが載せられており、その多くが Debian を基にしています。Distrowatch ウェブサイトを閲覧することはフリーソフトウェア世界の多様性を感じる素晴らしい方法です。<ulink type=\"block\" url=\"http://distrowatch.com\" />"

# Checked-By: Ryuunosuke Ayanokouzi;
msgid "The search form can help track down a distribution based on its ancestry. In March 2015, selecting Debian led to 131 active distributions! <ulink type=\"block\" url=\"http://distrowatch.com/search.php\" />"
msgstr "Distrowatch の検索フォームを使うと、祖先を基準にディストリビューションを見つけ出すことが可能です。2015 年 3 月の時点で、Debian を祖先に持つ活発なディストリビューションは 131 種あります!<ulink type=\"block\" url=\"http://distrowatch.com/search.php\" />"

#~ msgid "This interaction is becoming more common over time, thanks in part to the Ubuntu community and the efforts it makes in educating its new contributors. <ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"
#~ msgstr "Ubuntu コミュニティと新しい貢献者に対する教育に力を入れたことのおかげで、時がたつにつれこの種の直接的な交流は一般的なものになりつつあります。<ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"

#~ msgid "SimplyMEPIS"
#~ msgstr "SimplyMEPIS"

#~ msgid "<primary>SimplyMEPIS</primary>"
#~ msgstr "<primary>SimplyMEPIS</primary>"

#~ msgid "SimplyMEPIS is a commercial distribution very similar to Knoppix. It provides a turn-key Linux system from a LiveCD, and includes a number of non-free software packages: device drivers for nVidia video cards, Flash for animations embedded in many websites, RealPlayer, Sun's Java, and so on. The goal is to provide a 100 % working system out of the box. Mepis is internationalized and handles many languages. <ulink type=\"block\" url=\"http://www.mepis.org/\" />"
#~ msgstr "SimplyMEPIS は Knoppix とよく似た商用ディストリビューションです。SimplyMEPIS はライブ CD からすぐに使える Linux システムを提供し、数多くの自由ではないソフトウェアのパッケージを備えています。たとえば nVidia ビデオカードのデバイスドライバ、多くのウェブサイトに埋め込まれているアニメーション用の Flash、RealPlayer、Sun の Java などです。SimplyMEPIS の目標は難しい設定を一切することなく完璧に動作するシステムを提供することです。Mepis は国際化されており、多くの言語を取り扱うことが可能です。<ulink type=\"block\" url=\"http://www.mepis.org/\" />"

#~ msgid "This distribution was originally based on Debian; it went to Ubuntu for a while, then came back to Debian Stable, which allows its developers to focus on adding features without having to stabilize packages coming from Debian's <emphasis role=\"distribution\">Unstable</emphasis> distribution."
#~ msgstr "元来、SimplyMEPIS ディストリビューションは Debian を基にしていました。しばらくの間 Ubuntu を基にしていたこともありましたが、Debian 安定版に戻りました。こうすることで、SimplyMEPIS の開発者は Debian の<emphasis role=\"distribution\">不安定版</emphasis>ディストリビューションに含まれるパッケージの安定化ではなく機能の追加に集中することが可能になります。"
